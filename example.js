
const { LinkedInProfileScraper } = require('linkedin-profile-scraper')
const express = require('express');

const app = express();

(async () => {
  // Setup environment variables to fill the sessionCookieValue
  const scraper = new LinkedInProfileScraper({
    sessionCookieValue: `AQEDASyFZXoCVB4oAAABc8RxwbkAAAFz6H5FuVYAJB4LQJbwOCiSVwI_pk4jMfCD7s0UI9aisSC1aUudS9J6vgerZmkJ5gK9sbje-sJMnvfLg1hRbP_V6O-Qz3XMdRoh7leqxvhar6yfIi6y6z85gHEw`,
    keepAlive: true,
  })

  // Prepare the scraper
  // Loading it in memory
  await scraper.setup()

  // Usage: http://localhost:3000/?url=https://www.linkedin.com/in/jvandenaardweg/
  // Usage: http://localhost:3000/?url=https://www.linkedin.com/in/yohannravino/
  // Usage: http://localhost:3000/?url=https://www.linkedin.com/in/quentin-romain-8745b772/
  app.get('/', async (req, res) => {
    const urlToScrape = String(req.query.url);

    const result = await scraper.run(urlToScrape)

    return res.json(result)
  })

  app.listen(process.env.PORT || 3000)
})()

// https://www.linkedin.com/in/yohannravino/
/// AQEDASyFZXoCVB4oAAABc8RxwbkAAAFz6H5FuVYAJB4LQJbwOCiSVwI_pk4jMfCD7s0UI9aisSC1aUudS9J6vgerZmkJ5gK9sbje-sJMnvfLg1hRbP_V6O-Qz3XMdRoh7leqxvhar6yfIi6y6z85gHEw